//
//  MainGameController.m
//  BubblePop
//
//  Created by Luu Nguyen on 5/6/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "MainGameController.h"
#import "SettingsManager.h"
#import "HighScoreManager.h"
#import "BubbleView.h"
#include "BubblePoolView.h"

@interface MainGameController() <BubbleViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (nonatomic) NSInteger score;
@property (weak, nonatomic) IBOutlet UIView *popUp;

@property (weak, nonatomic) IBOutlet UILabel *countdown;
@property (weak, nonatomic) IBOutlet UILabel *timer;
@property (weak, nonatomic) IBOutlet UILabel *coin;

@property (nonatomic) NSTimer *countdownTimer;
@property (nonatomic) NSInteger countdownValue;

@property (nonatomic) NSTimer *remainTimer;
@property (nonatomic) NSInteger remainValue;

@property (weak, nonatomic) IBOutlet BubblePoolView *pool;

@property (nonatomic) NSInteger bubbleCount;
@property (nonatomic) NSInteger bubbleType;

@end

@implementation MainGameController

-(void)viewDidLoad
{
   _txtName.delegate = self;
   _pool.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
   _popUp.hidden = YES;
   _countdown.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
   [self startGame];
}

- (void)viewWillDisappear:(BOOL)animated
{
   [self stopGame];
}

- (BOOL)prefersStatusBarHidden
{
   return YES;
}

- (void)startGame
{
   [_pool setInteractive:NO];
   [_pool start];
   
   _coin.text = @"0";
   _remainValue = [SettingsManager maximumTime];
   _timer.text = [NSString stringWithFormat:@"%02d", _remainValue];

   _countdown.text = @"";
   _countdown.hidden = NO;
   _countdownValue = 3;
   _countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(startCountdown) userInfo:nil repeats:YES];
   
}

- (void)startCountdown
{
   _countdown.text = [NSString stringWithFormat:@"%d", _countdownValue];

   if(_countdownValue)
   {
      --_countdownValue;
   }
   else
   {
      [_countdownTimer invalidate];
      _countdown.hidden = YES;
      [_pool setInteractive:YES];
      _remainTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(remainCountdown) userInfo:nil repeats:YES];
   }
}

- (void)stopGame
{
   [_pool stop];
   [_countdownTimer invalidate];
   [_remainTimer invalidate];
   [self back];
}

- (IBAction)saveHighScore:(id)sender
{
   NSString *name = _txtName.text;
   if(!name || ![name length]) return;
   
   [[HighScoreManager sharedInstance] addHighScore:_score name:name];
   [[HighScoreManager sharedInstance] save];
   [self back];
}

- (void)back
{
   [self performSegueWithIdentifier:@"back" sender:self];
}

-(void)bubbleViewPopped:(BubbleView *)view
{
   if (_bubbleCount && _bubbleType == view.type) {
         _bubbleCount++;
      _score += 1.5f * view.score;
   } else {
      _bubbleCount = 1;
      _bubbleType = view.type;
      _score += view.score;
   }
   
   _coin.text = [NSString stringWithFormat:@"%d", _score];
}

- (void)remainCountdown
{
   _timer.text = [NSString stringWithFormat:@"%02d", _remainValue];
   if (_remainValue) {
      --_remainValue;
   } else {
      [_pool setInteractive:NO];
      [_pool stop];
      [_remainTimer invalidate];
      _popUp.hidden = NO;
   }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   [textField resignFirstResponder];
   
   return YES;
}

@end
