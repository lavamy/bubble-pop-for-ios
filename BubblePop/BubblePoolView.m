//
//  BubblePoolView.m
//  BubblePop
//
//  Created by Luu Nguyen on 5/9/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "BubblePoolView.h"
#import "SettingsManager.h"

@interface BubblePoolView() <BubbleViewDelegate>

@end

@implementation BubblePoolView
{
   NSTimer *_delayTimer;
   int _currentBubble;
}

- (void)start
{
   int size = [SettingsManager MaximumBubbles];
   _bubbles = [NSMutableArray array];

//   Red   1  40%
//   Pink  2  30%
//   Green 5  15%
//   Blue  8  10%
//   Black 10 5%
   
   int nRed = 0.4f * size;
   int nPink = 0.3f * size;
   int nGreen = 0.15f * size;
   int nBlue = 0.1f * size;
   int nBlack = size - (nRed + nPink + nGreen + nBlue);
   
   [self createBubbleType:1 count:nRed score:1 color:[[UIColor redColor] CGColor] speed:2];
   [self createBubbleType:2 count:nPink score:2 color:[[UIColor colorWithRed:0.96 green:0.29 blue:0.80 alpha:1.0] CGColor] speed:2.5];
   [self createBubbleType:3 count:nGreen score:5 color:[[UIColor greenColor] CGColor] speed:2.8];
   [self createBubbleType:4 count:nBlue score:8 color:[[UIColor blueColor] CGColor] speed:3];
   [self createBubbleType:5 count:nBlack score:10 color:[[UIColor colorWithRed:0.07 green:0.06 blue:0.06 alpha:1.0] CGColor] speed:3.4];
   
   for (int i = 0; i < _bubbles.count; ++i) {
      int j = arc4random_uniform(_bubbles.count);
      [_bubbles exchangeObjectAtIndex:i withObjectAtIndex:j];
   }
   
   _currentBubble = 0;
   _delayTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                  target:self
                                                selector:@selector(showBubbles) userInfo:nil repeats:YES];
}

- (void)showBubbles
{
   if (_currentBubble < _bubbles.count) {
      [[_bubbles objectAtIndexedSubscript:_currentBubble++] moveInRect:self.frame];
   }
   else {
      [_delayTimer invalidate];
   }
}

- (void)createBubbleType:(int)type count:(int)count score:(int)score color:(CGColorRef)color speed:(float)speed
{
   for (int i = 0; i < count; ++i)
   {
      BubbleView *bubble = [[BubbleView alloc] initWithType:type
                                                      color:color
                                                       size:30 + score * 3
                                                      score:score
                                                      speed:speed];
      bubble.delegate = self;
      bubble.pool = self;
      [bubble moveInRect:self.frame];
      [_bubbles addObject:bubble];
   }
}

- (void)stop
{
   for (int i = 0; i < _bubbles.count; ++i) {
      [_bubbles[i] stop];
   }
   
   [_bubbles removeAllObjects];
   [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
   for (int i = 0; i < _bubbles.count; ++i) {
      [_bubbles[i] drawInContext:UIGraphicsGetCurrentContext()];
   }
}

- (void)bubbleViewEntered:(BubbleView *)view
{
   if (_delegate) {
      [_delegate bubbleViewEntered:view];
   }
}

- (void)bubbleViewHidden:(BubbleView *)view
{
   if (_delegate) {
      [_delegate bubbleViewHidden:view];
   }
}

- (void)bubbleViewPopped:(BubbleView *)view
{
   if (_delegate) {
      [_delegate bubbleViewPopped:view];
   }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
   if(!_interactive) return;
   
   UITouch *touch = [touches anyObject];
   CGPoint pos = [touch locationInView:self];

   for (int i = 0; i < _bubbles.count; ++i) {
      BubbleView *bubble = [_bubbles objectAtIndexedSubscript:i];
      if ([bubble containsPoint:pos]) {
         [bubble pop];
      }
   }
}

@end
