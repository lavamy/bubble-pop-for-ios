//
//  BubbleView.m
//  BubblePop
//
//  Created by Luu Nguyen on 5/6/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "BubbleView.h"
#import "BubblePoolView.h"

@implementation BubbleView
{
   NSTimer *_timer;
   BOOL _stopped;
   CGRect _space;
   BubblePoolView *_castedPool;
}

- (instancetype)initWithType:(int)type color:(CGColorRef)color size:(float)size score:(int)score speed:(float)speed
{
   if (self = [super init]) {
      _type = type;
      _color = CGColorCreateCopyWithAlpha(color, 0.6f);
      _size = size;
      _score = score;
      _speed = speed;
   }
   return self;
}

- (void)setPool:(UIView *)pool
{
   _pool = pool;
   _castedPool = (BubblePoolView *)pool;
}

- (BOOL)containsPoint:(CGPoint)point
{
   float centerX = _bounds.origin.x + _size;
   float centerY = _bounds.origin.y + _size;
   
   return pow(centerX - point.x, 2) + pow(centerY - point.y, 2) < _size * _size;
}

- (void)drawInContext:(CGContextRef)ctx
{
   CGContextAddEllipseInRect(ctx, _bounds);
   CGContextSetFillColor(ctx, CGColorGetComponents(_color));
   CGContextFillPath(ctx);
}

- (void)moveInRect:(CGRect)rect
{
   [_timer invalidate];

   _stopped = NO;
   _popped = NO;
   _space = rect;
   [self respawn];

   _timer = [NSTimer scheduledTimerWithTimeInterval:0.0025f
                                             target:self selector:@selector(updatePosition)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)pop
{
   _popped = YES;

   if (_delegate) {
      [_delegate bubbleViewPopped:self];
   }
}

- (void)stop
{
   _stopped = YES;
}

- (void)updatePosition
{
   if (_popped) {
      float delta = _bounds.size.width * 0.4f;
      _bounds.size.width -= delta;
      _bounds.size.height -= delta;
      _bounds.origin.x += delta / 2;
      _bounds.origin.y += delta / 2;
      
      if (_bounds.size.width < 0.1f) {
         _popped = false;
         [self respawn];
      }
   }
   
   _bounds.origin.y -= _speed;
   
   if (_moveDirection)
   {
      _bounds.origin.x += _speed;
      if(_bounds.origin.x > _space.size.width - 2 * _size)
      {
         _bounds.origin.x = _space.size.width - 2 * _size;
         _moveDirection = !_moveDirection;
      }
   }
   else
   {
      _bounds.origin.x -= _speed;
      if(_bounds.origin.x < 0)
      {
         _bounds.origin.x = 0;
         _moveDirection = !_moveDirection;
      }
   }
   
   if (_bounds.origin.y < 0.0f) {
      [self respawn];

      if (_stopped) {
         [_timer invalidate];
      }
   }

   [_pool setNeedsDisplay];
}

- (void)respawn
{
   _bounds.origin.y = _space.size.height;
   _bounds.origin.x = arc4random_uniform(_space.size.width - _size * 2);
   _bounds.size.width = _bounds.size.height = _size * 2;
}

- (int)intersectWithBubble:(BubbleView *)bubble
{
   if (bubble.bounds.origin.y >= _space.size.height) {
      return 1;
   }
   
   double p = pow(_bounds.origin.x - bubble.bounds.origin.x, 2) + pow(_bounds.origin.y - bubble.bounds.origin.y, 2) - pow(_size + bubble.size, 2);
   if (fabs(p) < 0.001f) {
      return 0;
   } else return p > 0 ? 1 : -1;
}

@end
