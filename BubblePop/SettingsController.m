//
//  SettingsController.m
//  BubblePop
//
//  Created by Luu Nguyen on 5/6/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "SettingsController.h"
#import "SettingsManager.h"

@interface SettingsController()

@property (weak, nonatomic) IBOutlet UISwitch *switchAudio;
@property (weak, nonatomic) IBOutlet UISlider *sliderTime;
@property (weak, nonatomic) IBOutlet UISlider *sliderBubbles;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UILabel *lbBubbles;

@end

@implementation SettingsController

- (void)viewWillAppear:(BOOL)animated
{
   [_switchAudio setEnabled:[SettingsManager audioEnabled]];
   [_sliderTime setValue:[SettingsManager maximumTime]];
   [_sliderBubbles setValue:[SettingsManager MaximumBubbles]];

   _lbTime.text = [NSString stringWithFormat:@"%d", (int)_sliderTime.value];
   _lbBubbles.text = [NSString stringWithFormat:@"%d", (int)_sliderBubbles.value];
}

- (IBAction)audioEnabledChanged:(id)sender {
   [SettingsManager setAudioEnabled:_switchAudio.enabled];
}

- (IBAction)maximumTimeChanged:(id)sender {
   [SettingsManager setMaximumTime:(NSInteger)_sliderTime.value];
   _lbTime.text = [NSString stringWithFormat:@"%d", (int)_sliderTime.value];
}

- (IBAction)maximumBubblesChanged:(id)sender {
   [SettingsManager setMaximumBubbles:(NSInteger)_sliderBubbles.value];
   _lbBubbles.text = [NSString stringWithFormat:@"%d", (int)_sliderBubbles.value];
}

@end
