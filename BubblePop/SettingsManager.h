//
//  SettingsManager.h
//  BubblePop
//
//  Created by Luu Nguyen on 5/8/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsManager : NSObject

+ (void)registerDefaultsFromSettingsBundle;

+ (BOOL)audioEnabled;
+ (void)setAudioEnabled:(BOOL)enabled;

+ (NSInteger)maximumTime;
+ (NSInteger)MaximumBubbles;

+ (void)setMaximumTime:(NSInteger)time;
+ (void)setMaximumBubbles:(NSInteger)numberOfBubbles;

@end
