//
//  BubbleView.h
//  BubblePop
//
//  Created by Luu Nguyen on 5/6/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BubbleView;

@protocol BubbleViewDelegate <NSObject>

@optional

- (void)bubbleViewEntered:(BubbleView *)view;
- (void)bubbleViewHidden:(BubbleView *)view;

@required

- (void)bubbleViewPopped: (BubbleView *)view;

@end

@interface BubbleView : NSObject

@property (nonatomic) float size;
@property (nonatomic) CGColorRef color;
@property (nonatomic) NSInteger score;
@property (nonatomic) NSInteger type;
@property (nonatomic) float speed;
@property (nonatomic) id<BubbleViewDelegate> delegate;
@property (nonatomic) UIView *pool;
@property (nonatomic) CGRect bounds;
@property (nonatomic) BOOL popped;
@property (nonatomic) BOOL moveDirection;

- (instancetype)initWithType:(int)type color:(CGColorRef)color size:(float)size score:(int)score speed:(float)speed;

- (BOOL)containsPoint:(CGPoint)point;
- (void)drawInContext:(CGContextRef)context;
- (void)moveInRect:(CGRect)rect;
- (void)pop;
- (void)stop;
- (int)intersectWithBubble:(BubbleView *)bubble;

@end
