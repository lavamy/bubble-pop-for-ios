//
//  HighScoreManager.h
//  BubblePop
//
//  Created by Luu Nguyen on 5/8/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *kHighScoreName = @"highScoreName";
static NSString *kHighScoreValue = @"highScoreValue";
static NSString *kHighScoreCount = @"highScoreCount";

@interface HighScoreManager : NSObject

+ (HighScoreManager *)sharedInstance;

- (void)save;
- (BOOL)isHighScore:(uint)score;
- (void)addHighScore:(uint)score name:(NSString *)name;
- (NSDictionary *)retrieveHighScoreAt:(uint)position;

@end
