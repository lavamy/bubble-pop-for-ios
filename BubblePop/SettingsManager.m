//
//  SettingsManager.m
//  BubblePop
//
//  Created by Luu Nguyen on 5/8/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "SettingsManager.h"

static NSString *kAudioEnabled = @"soundEnabled";
static NSString *kTime = @"Time";
static NSString *kNumberOfBubbles = @"numberOfBubbles";

@implementation SettingsManager

+ (void)registerDefaultsFromSettingsBundle {
   NSInteger time = [[NSUserDefaults standardUserDefaults] integerForKey:kTime];
   if (time)
   {
      NSLog(@"Settings changed");
      return;
   }
   
   NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
   if(!settingsBundle)
   {
      NSLog(@"Could not find Settings.bundle");
      return;
   }
   
   NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
   NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
   
   NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
   for(NSDictionary *prefSpecification in preferences)
   {
      NSString *key = [prefSpecification objectForKey:@"Key"];
      if(key)
      {
         [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
         NSLog(@"writing as default %@ to the key %@",[prefSpecification objectForKey:@"DefaultValue"],key);
      }
   }
   
   [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
   [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)audioEnabled
{
   [[NSUserDefaults standardUserDefaults] synchronize] ;
   return [[NSUserDefaults standardUserDefaults] boolForKey:kAudioEnabled];
}

+ (void)setAudioEnabled:(BOOL)enabled
{
   [[NSUserDefaults standardUserDefaults] setBool:enabled forKey:kAudioEnabled];
   [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)maximumTime
{
   [[NSUserDefaults standardUserDefaults] synchronize];
   return [[NSUserDefaults standardUserDefaults] integerForKey:kTime];
}

+ (NSInteger)MaximumBubbles
{
   [[NSUserDefaults standardUserDefaults] synchronize];
   return [[NSUserDefaults standardUserDefaults] integerForKey:kNumberOfBubbles];
}

+ (void)setMaximumTime:(NSInteger)time
{
   [[NSUserDefaults standardUserDefaults] setInteger:time forKey:kTime];
   [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setMaximumBubbles:(NSInteger)numberOfBubbles
{
   [[NSUserDefaults standardUserDefaults] setInteger:numberOfBubbles forKey:kNumberOfBubbles];
   [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
