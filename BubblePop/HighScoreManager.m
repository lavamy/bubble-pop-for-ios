//
//  HighScoreManager.m
//  BubblePop
//
//  Created by Luu Nguyen on 5/8/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "HighScoreManager.h"

@implementation HighScoreManager
{
   NSUserDefaults *_userDefaults;
   NSMutableArray<NSDictionary *> *_highScores;
}

+ (HighScoreManager *)sharedInstance
{
   static HighScoreManager *instance = nil;
   static dispatch_once_t onceToken;
   
   dispatch_once(&onceToken, ^{
      instance = [[self alloc] init];
   });
   
   return instance;
}

- (instancetype)init
{
   if(self = [super init])
   {
      _userDefaults = [NSUserDefaults standardUserDefaults];
      _highScores = [NSMutableArray array];
      NSInteger size = [_userDefaults integerForKey:kHighScoreCount];
      for(int i = 0; i < size; ++i)
      {
         NSDictionary *highScore = [_userDefaults dictionaryForKey:[NSString stringWithFormat:@"%@%d", kHighScoreValue, i]];
         [_highScores addObject:highScore];
      }
   }
   return self;
}

- (NSDictionary *)retrieveHighScoreAt:(uint)position
{
   if(position >= [_highScores count]) return nil;
   return [_highScores objectAtIndexedSubscript:position];
}

- (BOOL)isHighScore:(uint)score
{
   if([_highScores count])
      return score > [[[_highScores objectAtIndexedSubscript:[_highScores count] - 1] objectForKeyedSubscript:kHighScoreValue] intValue];
   return YES;
}

- (void)addHighScore:(uint)score name:(NSString *)name
{
   NSDictionary *newHighScore = @{
                                  kHighScoreName:name,
                                  kHighScoreValue:[NSNumber numberWithInt:score]
                                  };
   for (int i = 0; i < [_highScores count]; ++i) {
      NSDictionary *highScore = [_highScores objectAtIndexedSubscript:i];

      if ([[highScore objectForKeyedSubscript:kHighScoreValue] intValue] < score)
      {
         [_highScores insertObject:newHighScore atIndex:i];
         return;
      }
   }

   [_highScores addObject:newHighScore];
}

- (void)save
{
   [_userDefaults setInteger:[_highScores count] forKey:kHighScoreCount];
   
   for (int i = 0; i < [_highScores count]; ++i)
   {
      NSDictionary *highScore = [_highScores objectAtIndexedSubscript:i];
      [_userDefaults setObject:highScore forKey:[NSString stringWithFormat:@"%@%d", kHighScoreValue, i]];
   }
   
   [_userDefaults synchronize];
}

@end
