//
//  SplashScreenController.m
//  BubblePop
//
//  Created by Luu Nguyen on 5/6/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "SplashScreenController.h"

@implementation SplashScreenController

- (void)viewDidAppear:(BOOL)animated {
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(showMenu) userInfo:nil repeats:NO];
}

- (void)showMenu {
    [self performSegueWithIdentifier:@"showMenu" sender:self];
}

@end
