//
//  BubblePoolView.h
//  BubblePop
//
//  Created by Luu Nguyen on 5/9/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BubbleView.h"

@interface BubblePoolView : UIView

@property (nonatomic) NSMutableArray *bubbles;
@property (nonatomic) BOOL interactive;
@property (nonatomic) id<BubbleViewDelegate> delegate;

- (void)start;
- (void)stop;

@end
