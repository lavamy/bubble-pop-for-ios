//
//  HighScoreController.m
//  BubblePop
//
//  Created by Luu Nguyen on 5/6/16.
//  Copyright © 2016 Uphave. All rights reserved.
//

#import "HighScoreController.h"
#import "HighScoreManager.h"

@interface HighScoreController()

@property (weak, nonatomic) IBOutlet UILabel *name1;
@property (weak, nonatomic) IBOutlet UILabel *name2;
@property (weak, nonatomic) IBOutlet UILabel *name3;
@property (weak, nonatomic) IBOutlet UILabel *name4;
@property (weak, nonatomic) IBOutlet UILabel *name5;

@property (weak, nonatomic) IBOutlet UILabel *score1;
@property (weak, nonatomic) IBOutlet UILabel *score2;
@property (weak, nonatomic) IBOutlet UILabel *score3;
@property (weak, nonatomic) IBOutlet UILabel *score4;
@property (weak, nonatomic) IBOutlet UILabel *score5;

@end

@implementation HighScoreController

- (void)viewWillAppear:(BOOL)animated
{
   HighScoreManager *highScoreManager = [HighScoreManager sharedInstance];
   
   NSDictionary *highScore1 = [highScoreManager retrieveHighScoreAt:0];
   if(highScore1)
   {
      [_name1 setText:[highScore1 objectForKeyedSubscript:kHighScoreName]];
      [_score1 setText:[NSString stringWithFormat:@"%@", [highScore1 objectForKeyedSubscript:kHighScoreValue]]];
   }
   else
   {
      [_name1 setText:@""];
      [_score1 setText:@""];
   }

   NSDictionary *highScore2 = [highScoreManager retrieveHighScoreAt:1];
   if(highScore2)
   {
      [_name2 setText:[highScore2 objectForKeyedSubscript:kHighScoreName]];
      [_score2 setText:[NSString stringWithFormat:@"%@", [highScore2 objectForKeyedSubscript:kHighScoreValue]]];
   }
   else
   {
      [_name2 setText:@""];
      [_score2 setText:@""];
   }
   
   NSDictionary *highScore3 = [highScoreManager retrieveHighScoreAt:2];
   if(highScore3)
   {
      [_name3 setText:[highScore3 objectForKeyedSubscript:kHighScoreName]];
      [_score3 setText:[NSString stringWithFormat:@"%@", [highScore3 objectForKeyedSubscript:kHighScoreValue]]];
   }
   else
   {
      [_name3 setText:@""];
      [_score3 setText:@""];
   }
   
   NSDictionary *highScore4 = [highScoreManager retrieveHighScoreAt:3];
   if(highScore4)
   {
      [_name4 setText:[highScore4 objectForKeyedSubscript:kHighScoreName]];
      [_score4 setText:[NSString stringWithFormat:@"%@", [highScore4 objectForKeyedSubscript:kHighScoreValue]]];
   }
   else
   {
      [_name4 setText:@""];
      [_score4 setText:@""];
   }
   
   NSDictionary *highScore5 = [highScoreManager retrieveHighScoreAt:4];
   if(highScore5)
   {
      [_name5 setText:[highScore5 objectForKeyedSubscript:kHighScoreName]];
      [_score5 setText:[NSString stringWithFormat:@"%@", [highScore5 objectForKeyedSubscript:kHighScoreValue]]];
   }
   else
   {
      [_name5 setText:@""];
      [_score5 setText:@""];
   }
}

@end
